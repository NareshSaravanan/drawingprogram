var assert = require('assert');
var Tool = require('./tool.js');
describe('Drawing Tool', function() {
  var width = 20,
    height = 5,
    drawingTool = new Tool(width, height);
  describe('Instantiation the Class', function() {

    it('Canvas Height Should be of Same as given Height', function() {
      assert.equal(drawingTool.canvas.height, height);
    });

    it('Canvas Width Should be of Same as given Width', function() {
      assert.equal(drawingTool.canvas.width, width);
    });

    it('Canvas coordinates length should be same of height + 2(including top and bottom)', function() {
      assert.equal(drawingTool.canvas.coordinates.length, height + 2);
    });

    it('Canvas draw to be similar', function() {
      assert.equal(drawingTool.draw(), `----------------------
|                    |
|                    |
|                    |
|                    |
|                    |
----------------------
`);
    });
  });


  describe('Draw Lines with input as L 1 2 6 2', function() {
    it('Creating a  new line', function() {
      drawingTool.replaceLineCoordinates(1, 2, 6, 2);
      assert.equal(drawingTool.draw(), `----------------------
|                    |
|xxxxxx              |
|                    |
|                    |
|                    |
----------------------
`);
    });
  });

  describe('Draw Lines with input as L 6 3 6 4', function() {
    it('Creating a  new line', function() {
      drawingTool.replaceLineCoordinates(6, 3, 6, 4);
      assert.equal(drawingTool.draw(), `----------------------
|                    |
|xxxxxx              |
|     x              |
|     x              |
|                    |
----------------------
`);
    });
  });

  describe('Draw Rectangle with input as R 14 1 18 3', function() {
    it('Creating a  reactangle', function() {
      drawingTool.replaceRectangleCoordinates(14, 1, 18, 3);
      assert.equal(drawingTool.draw(), `----------------------
|             xxxxx  |
|xxxxxx       x   x  |
|     x       xxxxx  |
|     x              |
|                    |
----------------------
`);
    });
  });

  describe('Paint with input as B 10 3 o ', function() {
    it('Filling the Character', function() {
      drawingTool.bucketFill(10, 3, 'o');
      assert.equal(drawingTool.draw(), `----------------------
|oooooooooooooxxxxxoo|
|xxxxxxooooooox   xoo|
|oooooxoooooooxxxxxoo|
|oooooxoooooooooooooo|
|oooooooooooooooooooo|
----------------------
`);
    });
  });
});