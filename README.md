# Drawing Program

## Requirements

For development, you will only need Node.js installed on your environement.

### Node

[Node](http://nodejs.org/) is really easy to install & now include [NPM](https://npmjs.org/).
You should be able to run the following command after the installation procedure
below.

    $ node --version
    v8.5.0

    $ npm --version
    v8.5.0

#### Node installation on OS X

You will need to use a Terminal. On OS X, you can find the default terminal in
`/Applications/Utilities/Terminal.app`.

Please install [Homebrew](http://brew.sh/) if it's not already done with the following command.

    $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

If everything when fine, you should run

    brew install node

#### Node installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

---

## Install

    $ git clone https://NareshSaravanan@bitbucket.org/NareshSaravanan/drawingprogram.git
    $ cd PROJECT
    $ open drawingTool.html in the browser

### Sample Input

Key in below values in browser
- C 20 4
- L 1 2 6 2
- L 6 3 6 4 `Change the values and click Draw again`
- R 14 1 18 3
- B 10 3 o

## Testing

    $ npm install --global mocha

## Sample test run

    $ mocha test.js
