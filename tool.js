class Tool {
  constructor(width, height) {
    this.canvas = {
      width,
      height
    };
    this.validateCanvasSize();
    this.initiateCoordinates();
    this.drawCanvas()
  }

  drawCanvas() {
    let x1 = 0;
    let y1 = 0;
    let x2 = this.canvas.width + 1;
    let y2 = this.canvas.height + 1;
    this.arrangeCoordinates(x1, y1, x1, y2, '|', true);
    this.arrangeCoordinates(x2, y1, x2, y2, '|', true);
    this.arrangeCoordinates(x1, y1, x2, y1, '-', true);
    this.arrangeCoordinates(x1, y2, x2, y2, '-', true);
  }

  validateCanvasSize() {
    if (this.canvas.width < 1 || this.canvas.height < 1) {
      throw new Error('The canvas parameters must be a positive number.');
    }
  }

  arrangeCoordinates(x1, y1, x2, y2, symbol, skipValidate) {
    if (!skipValidate) {
      this.validateCoordinate(x1, y1);
      this.validateCoordinate(x2, y2);
    }
    if (x1 == x2) { //vertical
      if (y1 > y2) {
        let _y1 = y1;
        y1 = y2;
        y2 = _y1;
      }
      for (let i = y1; i <= y2; i++) {
        this.canvas.coordinates[i][x1] = symbol;
      }
    } else if (y1 == y2) { //horizontal
      if (x1 > x2) {
        let _x1 = x1;
        x1 = x2;
        x2 = _x1;
      }
      for (let i = x1; i <= x2; i++) {
        this.canvas.coordinates[y1][i] = symbol;
      }
    } else {
      throw new Error('The coordinates do not form a straight line. Must be either {x1} equals {x2} or {y1} equals {y2}.');
    }
  }

  initiateCoordinates() {
    this.canvas.coordinates = [];
    while (this.canvas.coordinates.push([]) <= this.canvas.height + 1);
  }

  validateCoordinate(x, y) {
    if (x < 1 || y < 1 || x >= (this.canvas.width + 1) || y >= (this.canvas.height + 1)) {
      throw new Error('x: ' + x + ' and y: ' + y + ' is an invalid coordinate. The coordinates must be pointed inside the canvas. width: ' + this.canvas.width + ', height: ' + this.canvas.height);
    }
  }

  draw() {
    let screen = "";
    for (let y = 0; y <= this.canvas.height + 1; y++) {
      for (let x = 0; x <= this.canvas.width + 1; x++) {
        var coordinate = this.canvas.coordinates[y][x];
        screen += coordinate || " ";
      }
      screen += "\n";
    }
    return screen;
  }

  lineCoordinates(x1, y1, x2, y2) {
    return {
      x1: parseInt(document.getElementById(x1).value),
      y1: parseInt(document.getElementById(y1).value),
      x2: parseInt(document.getElementById(x2).value),
      y2: parseInt(document.getElementById(y2).value)
    };
  }

  replaceLineCoordinates(x1, y1, x2, y2) {
    this.arrangeCoordinates(x1, y1, x2, y2, 'x');
  }
  replaceRectangleCoordinates(x1, y1, x2, y2) {
    this.arrangeCoordinates(x1, y1, x2, y1, 'x');
    this.arrangeCoordinates(x1, y2, x2, y2, 'x');
    this.arrangeCoordinates(x1, y1, x1, y2, 'x');
    this.arrangeCoordinates(x2, y1, x2, y2, 'x');
  }

  bucketFill(x, y, replacement_color) {
    this.validateCoordinate(x, y);
    let target_color = this.canvas.coordinates[y][x];
    if (target_color === replacement_color) {
      return;
    }
    this._bucketFill(x, y, target_color, replacement_color);
  }
  _bucketFill(x, y, target_color, replacement_color) {
    if (target_color !== this.canvas.coordinates[y][x]) {
      return;
    }
    this.canvas.coordinates[y][x] = replacement_color;
    this._bucketFill(x, y + 1, target_color, replacement_color);
    this._bucketFill(x + 1, y, target_color, replacement_color);
    this._bucketFill(x, y - 1, target_color, replacement_color);
    this._bucketFill(x - 1, y, target_color, replacement_color);
  }

}

module.exports = Tool