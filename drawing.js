function drawCanvas() {
  var width = parseInt(document.getElementById("width").value),
    height = parseInt(document.getElementById("height").value);
  this.drawingCanvas = new Tool(width, height);
  document.getElementById("drawCanvas").innerHTML = this.drawingCanvas.draw();
  this.drawingLine = JSON.parse(JSON.stringify(this.drawingCanvas));
}


function drawLine() {
  Object.setPrototypeOf(this.drawingLine, Tool.prototype);
  let {
    x1,
    y1,
    x2,
    y2
  } = lineCoordinates("lx1", "ly1", "lx2", "ly2");
  this.drawingLine.replaceLineCoordinates(x1, y1, x2, y2);
  document.getElementById("drawLine").innerHTML = this.drawingLine.draw();
  this.drawingRectangle = JSON.parse(JSON.stringify(this.drawingLine));

}

function drawRectangle() {
  Object.setPrototypeOf(this.drawingRectangle, Tool.prototype);
  let {
    x1,
    y1,
    x2,
    y2
  } = lineCoordinates("rx1", "ry1", "rx2", "ry2");
  this.drawingRectangle.replaceRectangleCoordinates(x1, y1, x2, y2);
  document.getElementById("drawRectangle").innerHTML = this.drawingRectangle.draw();
  this.drawingWithPaint = JSON.parse(JSON.stringify(this.drawingRectangle))

}

function paintColor() {
  Object.setPrototypeOf(this.drawingWithPaint, Tool.prototype);
  let x = parseInt(document.getElementById("bx1").value),
    y = parseInt(document.getElementById("by1").value),
    replacement_color = document.getElementById("rcolor").value;
  this.drawingWithPaint.bucketFill(x, y, replacement_color);
  document.getElementById("paintColor").innerHTML = this.drawingWithPaint.draw();
}

function lineCoordinates(x1, y1, x2, y2) {
  return {
    x1: parseInt(document.getElementById(x1).value),
    y1: parseInt(document.getElementById(y1).value),
    x2: parseInt(document.getElementById(x2).value),
    y2: parseInt(document.getElementById(y2).value)
  };
}